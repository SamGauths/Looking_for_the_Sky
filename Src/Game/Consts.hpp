#ifndef CONSTS_HPP_INCLUDED
#define CONSTS_HPP_INCLUDED


/// Tile constants
#define SIZE_BLOCK 50 // Size of a single block
#define NUM_BLOCKS_WIDTH 20 // 1000 Blocks large
#define NUM_BLOCKS_HEIGHT 12 // 16 Blocks high

enum{EMPTY, FLOOR, L_WALL, R_WALL};

///===================================================================

enum{INTRO, HOME, MAIN_MEN, NEW_GAME, IN_GAME, MAP_LEVEL};

///===================================================================

#define LOADING_SCREEN()\
        SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);\
        if(time2A - time2B < 100){\
         SDL_RenderCopy(ren, t_loading, &scaleLoading[6], &posLoadingTxt);\
         SDL_RenderCopy(ren, t_loading, &scaleLoading[0], &posLoading);\
        }\
        if(time2A - time2B > 99 && time2A - time2B < 200){\
         SDL_RenderCopy(ren, t_loading, &scaleLoading[7], &posLoadingTxt);\
         SDL_RenderCopy(ren, t_loading, &scaleLoading[1], &posLoading);\
        }\
        if(time2A - time2B > 199 && time2A - time2B < 300){\
         SDL_RenderCopy(ren, t_loading, &scaleLoading[8], &posLoadingTxt);\
         SDL_RenderCopy(ren, t_loading, &scaleLoading[2], &posLoading);\
        }\
        if(time2A - time2B > 299 && time2A - time2B < 400){\
         SDL_RenderCopy(ren, t_loading, &scaleLoading[8], &posLoadingTxt);\
         SDL_RenderCopy(ren, t_loading, &scaleLoading[3], &posLoading);\
        }\
        if(time2A - time2B > 399 && time2A - time2B < 500){\
         SDL_RenderCopy(ren, t_loading, &scaleLoading[8], &posLoadingTxt);\
         SDL_RenderCopy(ren, t_loading, &scaleLoading[5], &posLoading);\
        }\
        if(time2A - time2B > 499){\
         time2B = time2A;\
        }\
        time3B = time2A;


#endif // CONSTS_HPP_INCLUDED
