#include <iostream>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>

#include "Consts.hpp"
#include "Menu.hpp"


Menu::Menu(SDL_Renderer *renderer)
{
    ren = renderer;

    s_intro = IMG_Load("ressources/sprites/intro.bmp");
    s_home = IMG_Load("ressources/sprites/home.png");
    s_menu = IMG_Load("ressources/sprites/mainMenu.png");

    t_intro = SDL_CreateTextureFromSurface(renderer, s_intro);
    t_home = SDL_CreateTextureFromSurface(renderer, s_home);
    t_menu = SDL_CreateTextureFromSurface(renderer, s_menu);

    posBkg.x = 0;
    posBkg.y = 0;
    posBkg.w = 800;
    posBkg.h = 600;

    posIntro.x = (800/2) - (210/2);
    posIntro.y = -210;
    posIntro.w = 210;
    posIntro.h = 210;

    posHome.x = 800-(320+10);
    posHome.y = 600-(23+10);
    posHome.w = 320;
    posHome.h = 23;

    scaleHome[0].x = 0;
    scaleHome[0].y = 0;
    scaleHome[0].w = 320;
    scaleHome[0].h = 240;
    scaleHome[1].x = 320;
    scaleHome[1].y = 0;
    scaleHome[1].w = 320;
    scaleHome[1].h = 23;

    posMenu[0].x = (800/2) - (85/2);
    posMenu[0].y = 230;
    posMenu[0].w = 85;
    posMenu[0].h = 12;
    posMenu[1].x = (800/2) - (98/2);
    posMenu[1].y = 230 + (10+12);
    posMenu[1].w = 98;
    posMenu[1].h = 12;
    posMenu[2].x = (800/2) - (60/2);
    posMenu[2].y = 230 + (10+12+5+17);
    posMenu[2].w = 60;
    posMenu[2].h = 17;
    posMenu[3].x = (800/2) - (112/2);
    posMenu[3].y = 230 + (10+12+5+17+10+12);
    posMenu[3].w = 112;
    posMenu[3].h = 12;
    posMenu[4].x = (800/2) - (32/2);
    posMenu[4].y = 230 + (10+12+5+17+10+12+20);
    posMenu[4].w = 32;
    posMenu[4].h = 12;
    posMenu[5].x = 0;
    posMenu[5].y = 0;
    posMenu[5].w = 30;
    posMenu[5].h = 30;
    posMenu[6].x = 0;
    posMenu[6].y = 0;
    posMenu[6].w = 30;
    posMenu[6].h = 30;

    scaleMenu[0].x = 0;
    scaleMenu[0].y = 0;
    scaleMenu[0].w = 401;
    scaleMenu[0].h = 300;
    scaleMenu[1].x = 401;
    scaleMenu[1].y = 0;
    scaleMenu[1].w = 85;
    scaleMenu[1].h = 12;
    scaleMenu[2].x = 401;
    scaleMenu[2].y = 14;
    scaleMenu[2].w = 98;
    scaleMenu[2].h = 12;
    scaleMenu[3].x = 401;
    scaleMenu[3].y = 28;
    scaleMenu[3].w = 60;
    scaleMenu[3].h = 17;
    scaleMenu[4].x = 401;
    scaleMenu[4].y = 46;
    scaleMenu[4].w = 112;
    scaleMenu[4].h = 12;
    scaleMenu[5].x = 401;
    scaleMenu[5].y = 60;
    scaleMenu[5].w = 32;
    scaleMenu[5].h = 12;
    scaleMenu[6].x = 401;
    scaleMenu[6].y = 75;
    scaleMenu[6].w = 30;
    scaleMenu[6].h = 30;
    scaleMenu[7].x = 431;
    scaleMenu[7].y = 75;
    scaleMenu[7].w = 30;
    scaleMenu[7].h = 30;

    timeA = 0;
    timeB = 0;
}


Menu::~Menu()
{

}


void Menu::introMenu(SDL_Event event, int *mainVar)
{
    timeA = SDL_GetTicks();

    if(posIntro.y < (600/2) - (210/2))
    {
    if(timeA - timeB > 10)
    {
            posIntro.y += 1;
            timeB = timeA;
    }
    }
    else
    {
      if(timeA - timeB > 2000)
      {
            timeB = timeA;
            *mainVar = HOME;
      }
    }

    SDL_SetRenderDrawColor(ren, 255, 255, 255, 255);
    SDL_RenderCopy(ren, t_intro, NULL, &posIntro);
}


void Menu::homeMenu(SDL_Event event, int *mainVar)
{
   timeA = SDL_GetTicks();


   switch(event.type)
   {
    case SDL_KEYUP:
        *mainVar = MAIN_MEN;
    break;
   }

   if(timeA - timeB < 1000)
   {
    SDL_RenderCopy(ren, t_home, &scaleHome[0], &posBkg);
   }
   if(timeA - timeB > 999 && timeA - timeB < 2000)
   {
    SDL_RenderCopy(ren, t_home, &scaleHome[0], &posBkg);
    SDL_RenderCopy(ren, t_home, &scaleHome[1], &posHome);
   }
   if(timeA - timeB > 1999)
   {
    SDL_RenderCopy(ren, t_home, &scaleHome[0], &posBkg);
    timeB = timeA;
   }
}



void Menu::mainMenu(SDL_Event event, int *mainVar, bool *mainLoop)
{
   SDL_RenderCopy(ren, t_menu, &scaleMenu[0], &posBkg);
   SDL_RenderCopy(ren, t_menu, &scaleMenu[1], &posMenu[0]);
   SDL_RenderCopy(ren, t_menu, &scaleMenu[2], &posMenu[1]);
   SDL_RenderCopy(ren, t_menu, &scaleMenu[3], &posMenu[2]);
   SDL_RenderCopy(ren, t_menu, &scaleMenu[4], &posMenu[3]);
   SDL_RenderCopy(ren, t_menu, &scaleMenu[5], &posMenu[4]);

   if(event.motion.x > posMenu[0].x && event.motion.x < (posMenu[0].x + posMenu[0].w) && event.motion.y > posMenu[0].y && event.motion.y < (posMenu[0].y + posMenu[0].h))
   {
       posMenu[5].x = posMenu[0].x - (30+10);
       posMenu[5].y = posMenu[0].y - 10;
       posMenu[6].x = (posMenu[0].x + posMenu[0].w) + 5;
       posMenu[6].y = posMenu[0].y - 10;
       SDL_RenderCopy(ren, t_menu, &scaleMenu[6], &posMenu[5]);
       SDL_RenderCopy(ren, t_menu, &scaleMenu[7], &posMenu[6]);
   }
   else if(event.motion.x > posMenu[1].x && event.motion.x < (posMenu[1].x + posMenu[1].w) && event.motion.y > posMenu[1].y && event.motion.y < (posMenu[1].y + posMenu[1].h))
   {
       posMenu[5].x = posMenu[1].x - (30+10);
       posMenu[5].y = posMenu[1].y - 10;
       posMenu[6].x = (posMenu[1].x + posMenu[1].w) + 5;
       posMenu[6].y = posMenu[1].y - 10;
       SDL_RenderCopy(ren, t_menu, &scaleMenu[6], &posMenu[5]);
       SDL_RenderCopy(ren, t_menu, &scaleMenu[7], &posMenu[6]);
   }
   else if(event.motion.x > posMenu[2].x && event.motion.x < (posMenu[2].x + posMenu[2].w) && event.motion.y > posMenu[2].y && event.motion.y < (posMenu[2].y + posMenu[2].h))
   {
       posMenu[5].x = posMenu[2].x - (30+10);
       posMenu[5].y = posMenu[2].y - 10;
       posMenu[6].x = (posMenu[2].x + posMenu[2].w) + 5;
       posMenu[6].y = posMenu[2].y - 10;
       SDL_RenderCopy(ren, t_menu, &scaleMenu[6], &posMenu[5]);
       SDL_RenderCopy(ren, t_menu, &scaleMenu[7], &posMenu[6]);
   }
   else if(event.motion.x > posMenu[3].x && event.motion.x < (posMenu[3].x + posMenu[3].w) && event.motion.y > posMenu[3].y && event.motion.y < (posMenu[3].y + posMenu[3].h))
   {
       posMenu[5].x = posMenu[3].x - (30+10);
       posMenu[5].y = posMenu[3].y - 10;
       posMenu[6].x = (posMenu[3].x + posMenu[3].w) + 5;
       posMenu[6].y = posMenu[3].y - 10;
       SDL_RenderCopy(ren, t_menu, &scaleMenu[6], &posMenu[5]);
       SDL_RenderCopy(ren, t_menu, &scaleMenu[7], &posMenu[6]);
   }
   else if(event.motion.x > posMenu[4].x && event.motion.x < (posMenu[4].x + posMenu[4].w) && event.motion.y > posMenu[4].y && event.motion.y < (posMenu[4].y + posMenu[4].h))
   {
       posMenu[5].x = posMenu[4].x - (30+10);
       posMenu[5].y = posMenu[4].y - 10;
       posMenu[6].x = (posMenu[4].x + posMenu[4].w) + 5;
       posMenu[6].y = posMenu[4].y - 10;
       SDL_RenderCopy(ren, t_menu, &scaleMenu[6], &posMenu[5]);
       SDL_RenderCopy(ren, t_menu, &scaleMenu[7], &posMenu[6]);
   }

   switch(event.type)
   {
      case SDL_MOUSEBUTTONUP:
        if (event.button.button == SDL_BUTTON_LEFT)
        {
          if(event.motion.x > posMenu[0].x && event.motion.x < (posMenu[0].x + posMenu[0].w) && event.motion.y > posMenu[0].y && event.motion.y < (posMenu[0].y + posMenu[0].h))
          {
              *mainVar = NEW_GAME;
          }
        }
        if (event.button.button == SDL_BUTTON_LEFT)
        {
          if(event.motion.x > posMenu[4].x && event.motion.x < (posMenu[4].x + posMenu[4].w) && event.motion.y > posMenu[4].y && event.motion.y < (posMenu[4].y + posMenu[4].h))
          {
              *mainLoop = false;
          }
        }
      break;
   }
}



