#include <iostream>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>
#include <SDL_ttf.h>

#include "Consts.hpp"
#include "Menu.hpp"
#include "Game.hpp"
#include "Maps.hpp"
#include "Character.hpp"

using namespace std;

void quit(SDL_Event event, bool *mainLoop)
{
    switch(event.type)
    {
      case SDL_QUIT:
          *mainLoop = false;
       break;
    }
}

int main(int argc, char *argv[])
{
    freopen("CON", "w", stdout);
    freopen("CON", "r", stdin);
    freopen("CON", "w", stderr);

    bool mainLoop = true;
    int mainVar = INTRO;
    int Timer_Alpha___ = 0, x_char = 0;

    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_TIMER);
    TTF_Init();

    SDL_Window *screen = SDL_CreateWindow("Looking for the Sky", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 800, 600, SDL_WINDOW_SHOWN);
    SDL_SetWindowIcon(screen, IMG_Load("ressources/sprites/logo2.bmp"));
    SDL_Renderer *renderer = SDL_CreateRenderer(screen, -1, SDL_RENDERER_ACCELERATED);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);

    SDL_Event event;

    SDL_Rect posBkg = {0,0,800,600};

    Menu men(renderer);
    Game game(renderer);
    Maps levelMap(renderer);
    Character__ mainChar(renderer, "ressources/sprites/map1.bmp", "Player1", 100, ((600/2)-(90/2)), 60, 90);
    Character__ Second_One____(renderer, "ressources/character_sheet2.png", "Player2", 200, 200, 118, 184);

    while(mainLoop)
    {
        Timer_Alpha___ = SDL_GetTicks();

        SDL_PollEvent(&event);

        quit(event, &mainLoop); /// Quit function

            SDL_RenderFillRect(renderer, &posBkg);

            switch(mainVar)
            {
                  case INTRO:
                     men.introMenu(event, &mainVar);
                  break;

                  case HOME:
                     men.homeMenu(event, &mainVar);
                  break;

                  case MAIN_MEN:
                     men.mainMenu(event, &mainVar, &mainLoop);
                  break;

                  case NEW_GAME:
                     game.transitionToGame(event, &mainVar);
                  break;

                  case IN_GAME:
                    game.transitionToGame2(event, &mainVar);
                  break;

                  case MAP_LEVEL:
                    levelMap.currentMap(event, &mainVar, &x_char);
                    mainChar.Handle_Events__(event, &x_char);
                    mainChar.Update__(&Timer_Alpha___);
                    mainChar.Render__();
                  break;
            }

         SDL_RenderPresent(renderer);
        }

      SDL_DestroyWindow(screen);
      SDL_Quit();
      IMG_Quit();
      TTF_Quit();

    return 0;
}
