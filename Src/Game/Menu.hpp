#ifndef MENU_HPP_INCLUDED
#define MENU_HPP_INCLUDED

class Menu
{
   public:
     Menu(SDL_Renderer *renderer);
     ~Menu();
     void introMenu(SDL_Event event, int *mainVar);
     void homeMenu(SDL_Event event, int *mainVar);
     void mainMenu(SDL_Event event, int *mainVar, bool *mainLoop);

   private:
     SDL_Renderer *ren;
     SDL_Event event;
     SDL_Surface *s_intro;
     SDL_Texture *t_intro;
     SDL_Surface *s_home;
     SDL_Texture *t_home;
     SDL_Surface *s_menu;
     SDL_Texture *t_menu;
     SDL_Rect posBkg;
     SDL_Rect posIntro;
     SDL_Rect posHome;
     SDL_Rect scaleHome[2];
     SDL_Rect posMenu[7];
     SDL_Rect scaleMenu[8];
     int timeA;
     int timeB;
};

#endif // MENU_HPP_INCLUDED
