#ifndef MAPS_HPP_INCLUDED
#define MAPS_HPP_INCLUDED

class Maps
{
   public:
     Maps(SDL_Renderer *renderer);
     ~Maps();
     void currentMap(SDL_Event event, int *mainVar, int *x_char);

   private:
     SDL_Renderer *ren;
     SDL_Event event;
     SDL_Surface *s_tile[10];
     SDL_Texture *t_tile[10];
     SDL_Rect posBkg;
     SDL_Rect posTiles;
     SDL_Rect posT[10];
     int i = 0;
     int j = 0;
};


#endif // MAPS_HPP_INCLUDED
