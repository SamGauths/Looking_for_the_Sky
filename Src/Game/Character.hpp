#ifndef CHARACTER_HPP_INCLUDED
#define CHARACTER_HPP_INCLUDED

#include <string>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>

enum Movement_State
{
	STANDING_LEFT,
	STANDING_RIGHT,
	WALKING_LEFT,
	WALKING_RIGHT,
	RUNNING_LEFT,
	RUNNING_RIGHT,
	ATTACK_LEFT,
	ATTACK_RIGHT,
	JUMP_LEFT,
	JUMP_RIGHT,
	ATTACK_JUMP_LEFT,
	ATTACK_JUMP_RIGHT,
	CROUCH_LEFT,
	CROUCH_RIGHT
};

class Character__
{
public:
	Character__(SDL_Renderer *Renderer____, std::string, std::string, int, int, int, int);
	virtual ~Character__();
	virtual void Update__(int *Timer_A____);
	virtual void Handle_Events__(SDL_Event, int *x_char);
	virtual void Render__();
private:
    SDL_Renderer *ren;
    SDL_Rect posBkg;
    SDL_Rect posChar;
    SDL_Rect scaleChar;
	int X_Frame_Size___;
	int Y_Frame_Size___;
	int timeA;
	int timeB;
	SDL_Surface* Surface___;
	SDL_Texture* Texture___;
	Movement_State Movement_State___;
	std::string Name___;
	int Life___;
};


#endif // CHARACTER_HPP_INCLUDED
