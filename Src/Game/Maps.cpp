#include <iostream>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>
#include <SDL_ttf.h>

#include "Consts.hpp"
#include "Maps.hpp"


Maps::Maps(SDL_Renderer *renderer)
{
    ren = renderer;

    s_tile[0] = IMG_Load("ressources/sprites/sprite_sheet.bmp");
	t_tile[0] = SDL_CreateTextureFromSurface(ren, s_tile[0]);

	s_tile[1] = IMG_Load("ressources/sprites/sprite_sheet.bmp");
	t_tile[1] = SDL_CreateTextureFromSurface(ren, s_tile[1]);

    posBkg.x = 0;
    posBkg.y = 0;
    posBkg.w = 800;
    posBkg.h = 600;

    posTiles.w = 50;
    posTiles.h = 50; // 16 blocks high

    posT[0].x = 384; // Tile 1 (floor)
    posT[0].y = 32;
    posT[0].w = 32;
    posT[0].h = 32;

    posT[1].x = (384+32); // Tile 1 (floor)
    posT[1].y = 32;
    posT[1].w = 32;
    posT[1].h = 32;

    i = 0;
    j = 0;
}

Maps::~Maps(){}

/// TILE MAPPING
///===================================================================
FILE* fichier = fopen("ressources/files/level1.txt", "r");
int mapLevel[20][12] = {0};
char lineFile[NUM_BLOCKS_WIDTH * NUM_BLOCKS_HEIGHT + 1] = {0};
///===================================================================

void Maps::currentMap(SDL_Event event, int *mainVar, int *x_char)
{
    SDL_SetRenderDrawColor(ren, 0, 0, 0, 255); /// Black Background

    fgets(lineFile, NUM_BLOCKS_WIDTH * NUM_BLOCKS_HEIGHT + 1, fichier);

    for (i = 0 ; i < NUM_BLOCKS_WIDTH; i++)  // If the map is not 1000 large
    {
     for (j = 0 ; j < NUM_BLOCKS_HEIGHT; j++)  // If the map is not 16 high
     {
        posTiles.x = i * SIZE_BLOCK + *x_char; // X position of a block
        posTiles.y = j * SIZE_BLOCK; // Y position of a block

        switch(lineFile[(i * NUM_BLOCKS_HEIGHT) + j])
        {
            case '0':
                /// EMPTY BLOCK
                mapLevel[i][j] = EMPTY;
                break;

            case '1':
                SDL_RenderCopy(ren, t_tile[0], &posT[0], &posTiles);
                mapLevel[i][j] = EMPTY;
                break;

            case '2':
                SDL_RenderCopy(ren, t_tile[1], &posT[1], &posTiles);
                mapLevel[i][j] = EMPTY;
                break;
        }
     }
    }
}
