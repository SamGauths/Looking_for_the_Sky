#include <iostream>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>
#include <SDL_ttf.h>

#include "Consts.hpp"
#include "Game.hpp"


Game::Game(SDL_Renderer *renderer)
{
     ren = renderer;

     s_map1 = IMG_Load("ressources/sprites/map1.bmp");
     s_loading = IMG_Load("ressources/sprites/loading2.bmp");
     s_golds = IMG_Load("ressources/sprites/sprite_sheet.bmp");
     s_lvlSelect = IMG_Load("ressources/sprites/lvl_selection.png");
     s_select = IMG_Load("ressources/sprites/select.png");

     t_map1 = SDL_CreateTextureFromSurface(renderer, s_map1);
     t_loading = SDL_CreateTextureFromSurface(renderer, s_loading);
     t_golds = SDL_CreateTextureFromSurface(renderer, s_golds);
     t_lvlSelect = SDL_CreateTextureFromSurface(renderer, s_lvlSelect);
     t_select = SDL_CreateTextureFromSurface(renderer, s_select);

     i_golds = 0;
     if(i_golds < 10)
     {
        sprintf(c_golds, "0000%d", i_golds);
     }
     else if(i_golds > 9 && i_golds < 100)
     {
        sprintf(c_golds, "000%d", i_golds);
     }
     else if(i_golds > 99 && i_golds < 1000)
     {
        sprintf(c_golds, "00%d", i_golds);
     }
     else if(i_golds > 999 && i_golds < 10000)
     {
        sprintf(c_golds, "0%d", i_golds);
     }
     else if(i_golds > 9999)
     {
        sprintf(c_golds, "%d", i_golds);
     }
     police = TTF_OpenFont("arial.ttf", 64);
     texte = TTF_RenderText_Blended(police, c_golds, whiteColor);
     t_texte = SDL_CreateTextureFromSurface(renderer, texte);

     posBkg.x = 0;
     posBkg.y = 0;
     posBkg.w = 800;
     posBkg.h = 600;

     posLoading.x = (800-74);
     posLoading.y = (600-74);
     posLoading.w = 64;
     posLoading.h = 64;

     posLoadingTxt.x = (800 - (200 + 74));
     posLoadingTxt.y = (600-64);
     posLoadingTxt.w = 200;
     posLoadingTxt.h = 48;

     scaleLoading[0].x = 0;
     scaleLoading[0].y = 0;
     scaleLoading[0].w = 32;
     scaleLoading[0].h = 32;

     scaleLoading[1].x = 32;
     scaleLoading[1].y = 0;
     scaleLoading[1].w = 32;
     scaleLoading[1].h = 32;

     scaleLoading[2].x = 64;
     scaleLoading[2].y = 0;
     scaleLoading[2].w = 32;
     scaleLoading[2].h = 32;

     scaleLoading[3].x = 96;
     scaleLoading[3].y = 0;
     scaleLoading[3].w = 32;
     scaleLoading[3].h = 32;

     scaleLoading[4].x = 128;
     scaleLoading[4].y = 0;
     scaleLoading[4].w = 32;
     scaleLoading[4].h = 32;

     scaleLoading[5].x = 160;
     scaleLoading[5].y = 0;
     scaleLoading[5].w = 32;
     scaleLoading[5].h = 32;

     scaleLoading[6].x = 193;
     scaleLoading[6].y = 0;
     scaleLoading[6].w = 98;
     scaleLoading[6].h = 32;

     scaleLoading[7].x = 293;
     scaleLoading[7].y = 0;
     scaleLoading[7].w = 98;
     scaleLoading[7].h = 32;

     scaleLoading[8].x = 393;
     scaleLoading[8].y = 0;
     scaleLoading[8].w = 98;
     scaleLoading[8].h = 32;

     posGolds.x = 50;
     posGolds.y = 5;
     posGolds.w = 70;
     posGolds.h = 40;

     scaleG.x = 1887;
     scaleG.y = 704;
     scaleG.w = 32;
     scaleG.h = 32;

     posG.x = 130;
     posG.y = 10;
     posG.w = 32;
     posG.h = 32;

     posLvlSelect.x = 100;
     posLvlSelect.y = 100;
     posLvlSelect.w = 64;
     posLvlSelect.h = 64;

     posSelect.x = (100-10);
     posSelect.y = (100-10);
     posSelect.w = 84;
     posSelect.h = 84;

     scaleLvlSelect.x = 0;
     scaleLvlSelect.y = 0;
     scaleLvlSelect.w = 32;
     scaleLvlSelect.h = 32;

     timeA = 0;
     timeB = 0;
     time2A = 0;
     time2B = 0;
}

Game::~Game()
{

}

void Game::transitionToGame(SDL_Event event, int *mainVar)
{
    timeA = SDL_GetTicks();
    time2A = SDL_GetTicks();
    time3A = SDL_GetTicks();

    if(timeB == 0)
     timeB = timeA;

    if(time2B == 0)
     time2B = time2A;

    if(timeA - timeB < 5000)
    {
        LOADING_SCREEN()
    }
    if(timeA - timeB > 4999)
    {
        SDL_RenderCopy(ren, t_map1, NULL, &posBkg);
        SDL_RenderCopy(ren, t_texte, NULL, &posGolds);
        SDL_RenderCopy(ren, t_golds, &scaleG, &posG);
        if(time3A - time3B < 500)
        {
          SDL_RenderCopy(ren, t_lvlSelect, &scaleLvlSelect, &posLvlSelect);
        }
        if(time3A - time3B > 999)
        {
            time3B = time3A;
        }
        SDL_RenderCopy(ren, t_select, NULL, &posSelect);
    }

    switch(event.type)
    {
       case SDL_KEYUP:
           switch(event.key.keysym.sym)
           {
            case SDLK_SPACE:
                timeB = timeA;
                time2B = time2A;
                *mainVar = IN_GAME;
             break;
           }
        break;
    }
}

void Game::transitionToGame2(SDL_Event event, int *mainVar)
{
    timeA = SDL_GetTicks();
    time2A = SDL_GetTicks();

    if(timeA - timeB < 5000)
    {
        LOADING_SCREEN()
    }
    if(timeA - timeB > 4999)
    {
      *mainVar = MAP_LEVEL;
    }
}
