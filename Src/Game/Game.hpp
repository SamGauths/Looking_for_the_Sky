#ifndef GAME_HPP_INCLUDED
#define GAME_HPP_INCLUDED


class Game
{
   public:
     Game(SDL_Renderer *renderer);
     ~Game();
     void transitionToGame(SDL_Event event, int *mainVar);
     void transitionToGame2(SDL_Event event, int *mainVar);

   private:
     SDL_Renderer *ren;
     SDL_Event event;
     SDL_Surface *s_loading;
     SDL_Surface *s_map1;
     SDL_Surface *s_golds;
     SDL_Surface *s_lvlSelect;
     SDL_Surface *s_select;
     SDL_Texture *t_map1;
     SDL_Texture *t_loading;
     SDL_Texture *t_golds;
     SDL_Texture *t_lvlSelect;
     SDL_Texture *t_select;
     TTF_Font *police;
     SDL_Color whiteColor = {255, 255, 255};
     SDL_Surface *texte;
     SDL_Texture *t_texte;
     SDL_Rect posBkg;
     SDL_Rect posLoading;
     SDL_Rect posLoadingTxt;
     SDL_Rect scaleLoading[9];
     SDL_Rect posGolds;
     SDL_Rect scaleG;
     SDL_Rect posG;
     SDL_Rect posLvlSelect;
     SDL_Rect scaleLvlSelect;
     SDL_Rect posSelect;
     int timeA;
     int timeB;
     int time2A;
     int time2B;
     int time3A;
     int time3B;
     int i_golds;
     char c_golds[5];
};



#endif // GAME_HPP_INCLUDED
