#include "Character.hpp"

Character__::Character__(SDL_Renderer *Renderer____,
                         std::string Sprite_Sheet____,
						 std::string Name____,
	                     int Position_X____,
	                     int Position_Y____,
	                     int Size_X____,
	                     int Size_Y____)
:
Surface___(IMG_Load(Sprite_Sheet____.c_str())),
Name___(Name____),
Life___(0)
{
    ren = Renderer____;

	posChar.x = Position_X____;
    posChar.y = Position_Y____;
    posChar.w = 60;
    posChar.h = 90;

    X_Frame_Size___ = Size_X____;
    Y_Frame_Size___ = Size_Y____;
	Movement_State___ = STANDING_RIGHT;

    scaleChar.x = 0;
    scaleChar.y = 185;
    scaleChar.w = 120;
    scaleChar.h = 185;

    Surface___ = IMG_Load("ressources/sprites/character_sheet2.png");
	Texture___ = SDL_CreateTextureFromSurface(ren, Surface___);
}

Character__::~Character__(){}

void Character__::Update__(int *Timer_A____)
{
	timeA = *Timer_A____;

	if(timeB == 0)
    {
        timeB = timeA;
    }

	switch(Movement_State___)
	{
		case STANDING_LEFT:
			scaleChar.x = 0;
			scaleChar.y = 0;
			scaleChar.w = 120;
			scaleChar.h = 185;
			break;
		case STANDING_RIGHT:
			scaleChar.x = 0;
			scaleChar.y = 185;
			scaleChar.w = 120;
			scaleChar.h = 185;
			break;
        case CROUCH_LEFT:
			scaleChar.x = 0;
			scaleChar.y = 375;
			scaleChar.w = 120;
			scaleChar.h = 185;
			break;
        case CROUCH_RIGHT:
			scaleChar.x = 240;
			scaleChar.y = 375;
			scaleChar.w = 120;
			scaleChar.h = 185;
			break;
		case WALKING_LEFT:
		    if(timeA - timeB < 100)
            {
               scaleChar.x = 0;
               scaleChar.y = 0;
               scaleChar.w = 120;
               scaleChar.h = 185;
            }
            if(timeA - timeB > 99 && timeA - timeB < 200)
            {
               scaleChar.x = 112;
               scaleChar.y = 0;
               scaleChar.w = 120;
               scaleChar.h = 185;
            }
            if(timeA - timeB > 199 && timeA - timeB < 300)
            {
               scaleChar.x = 230;
               scaleChar.y = 0;
               scaleChar.w = 120;
               scaleChar.h = 185;
            }
            if(timeA - timeB > 299 && timeA - timeB < 400)
            {
               scaleChar.x = 354;
               scaleChar.y = 0;
               scaleChar.w = 120;
               scaleChar.h = 185;
            }
            if(timeA - timeB > 399 && timeA - timeB < 500)
            {
               scaleChar.x = 472;
               scaleChar.y = 0;
               scaleChar.w = 120;
               scaleChar.h = 185;
            }
            if(timeA - timeB > 499 && timeA - timeB < 600)
            {
               scaleChar.x = 584;
               scaleChar.y = 0;
               scaleChar.w = 120;
               scaleChar.h = 185;
            }
            if(timeA - timeB > 599)
            {
               timeB = timeA;
            }
			break;
		case WALKING_RIGHT:
		        if(timeA - timeB < 100)
            {
               scaleChar.x = 0;
               scaleChar.y = 185;
               scaleChar.w = 120;
               scaleChar.h = 185;
            }
            if(timeA - timeB > 99 && timeA - timeB < 200)
            {
               scaleChar.x = 112;
               scaleChar.y = 185;
               scaleChar.w = 120;
               scaleChar.h = 185;
            }
            if(timeA - timeB > 199 && timeA - timeB < 300)
            {
               scaleChar.x = 230;
               scaleChar.y = 185;
               scaleChar.w = 120;
               scaleChar.h = 185;
            }
            if(timeA - timeB > 299 && timeA - timeB < 400)
            {
               scaleChar.x = 354;
               scaleChar.y = 185;
               scaleChar.w = 120;
               scaleChar.h = 185;
            }
            if(timeA - timeB > 399 && timeA - timeB < 500)
            {
               scaleChar.x = 472;
               scaleChar.y = 185;
               scaleChar.w = 120;
               scaleChar.h = 185;
            }
            if(timeA - timeB > 499 && timeA - timeB < 600)
            {
               scaleChar.x = 584;
               scaleChar.y = 185;
               scaleChar.w = 120;
               scaleChar.h = 185;
            }
            if(timeA - timeB > 599)
            {
               timeB = timeA;
            }
			break;

		// case RUNNING_LEFT:
		// 	break;
		// case RUNNING_RIGHT:
		// 	break;
		// case ATTACK_LEFT:
		// 	break;
		// case ATTACK_RIGHT:
		// 	break;
		// case JUMP_LEFT:
		// 	break;
		// case JUMP_RIGHT:
		// 	break;
		// case ATTACK_JUMP_LEFT:
		// 	break;
		// case ATTACK_JUMP_RIGHT:
		// 	break;
	}
}

void Character__::Handle_Events__(SDL_Event Event____, int *x_char)
{
	switch(Event____.type)
	{
       case SDL_KEYDOWN:
	    switch(Event____.key.keysym.sym)
	    {
	      case SDLK_LEFT:
                Movement_State___ = WALKING_LEFT;
                *x_char += 1;
                SDL_Delay(2);
          break;

          case SDLK_RIGHT:
                Movement_State___ = WALKING_RIGHT;
                *x_char -= 1;
                SDL_Delay(2);
          break;

          case SDLK_DOWN:
              if(Movement_State___ == STANDING_LEFT)
              {
                Movement_State___ = CROUCH_LEFT;
              }
              else if(Movement_State___ == STANDING_RIGHT)
              {
                Movement_State___ = CROUCH_RIGHT;
              }
          break;
	    }
        break;

        case SDL_KEYUP:
	    switch(Event____.key.keysym.sym)
	    {
          case SDLK_LEFT:
              Movement_State___ = STANDING_LEFT;
            break;
          case SDLK_RIGHT:
              Movement_State___ = STANDING_RIGHT;
            break;
          case SDLK_DOWN:
              if(Movement_State___ == CROUCH_LEFT)
              {
                Movement_State___ = STANDING_LEFT;
              }
              else if(Movement_State___ == CROUCH_RIGHT)
              {
                Movement_State___ = STANDING_RIGHT;
              }
            break;
	    }
        break;
	}
}

void Character__::Render__()
{
	SDL_RenderCopy(ren, Texture___, &scaleChar, &posChar);
}
